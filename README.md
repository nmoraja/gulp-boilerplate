# Gulp 2022 boilerplate
A boilerplate for building web projects with Gulp.

## Features
- Running developer server.
- Watch for file changes, and automatically recompile build and reload webpages.
- Working with `html` or `pug` files.
- Compile, minify, autoprefix, and lint Sass.
- Concatenate, minify, and lint JavaScript.
- Optimize images and create .webp files.
- Create svg sprite file.
- Create woff2 and woff fonts files.
- Copy static files and folders into your dist directory.
- Automatically add version info to JS and CSS files.

## Getting up and running
1. Create project directory.
2. Clone this repo.
3. Run `npm install` to install required dependencies.
4. Run `npm run dev`. Your browser will automatically be opened.
1. To prepare assets to for production run the `npm run build`.
2. To prepare zip file run the `npm run zip`
3. To deploy `/dist` folder on the ftp server run the `npm run deploy`

## Structure
Add your source files to the `/src` subcategories. Gulp will process and compile them into `/dist` subcategory.
- Javascript files in the `/src/js`. Main file is `app.js`. Files in subcategories under the `js` folder will be concatenated and added to `/dist/js` folder.
- Files in the `/src/scss` directory will be compiled to `/dist/css` folder.
- Files from `/src/files` will be copied to `/dist/files` folder.
- Fonts in format `ttf` and `otf` from `/src/fonts` will be transform to `woff2` and `woff` format and copied to `/dist/fonts`.
- Images from `/src/img` will be optimized and copied to `/dist/img`,
- If you run `npm run svgScript` you create icons svg sprite file and put in the `/dist/img`
- `gulp/` - Gulp config, plugins and tasks files.
- `gulpfile.js` - Gulp main file.

## Scripts
- `npm run dev` - developer mode
`npm run build` - production mode
`nom run svgSprive` - create svg sprite file
`npm run zip` - create `dist` folder zip archive
`npm run deploy` - send `dist` folder files to FTP server (
requires configuration data)